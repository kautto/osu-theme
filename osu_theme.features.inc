<?php
/**
 * @file
 * osu_theme.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osu_theme_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}
