<?php
/**
 * @file
 * Template for single-column layout.
 *
 */

 
 ?>

<div class="panel-display panel-osu-1col clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
    <div class="panel-panel panel-content col-full">
        <div class="inside"><?php print $content['content']; ?></div>
    </div>
</div>
