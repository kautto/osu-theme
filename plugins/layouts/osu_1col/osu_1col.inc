<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('1 Full-width'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_1col',
  'regions' => array(
    'content' => t('Content column')
  ),
);


