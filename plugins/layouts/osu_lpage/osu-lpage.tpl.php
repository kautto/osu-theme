<?php
/**
 * @file
 * Template for the 2 fullwidth row + 3 column panel layout for landing pages.
 *
 *
 * Below we determine additional CSS classes that are added in to adjust
 * the width of elements in case the sidebar(s) are empty.
 */

$add_css_classes = "";

if (!$content['sidebar_1'] || !$content['sidebar_2']) {

    if ((!$content['sidebar_1']) && (!$content['sidebar_2'])) {
        $add_css_classes = " panel-osu-lpage-no-sidebars";
    }
    else if (!$content['sidebar_1']) {
        $add_css_classes = " panel-osu-lpage-no-sidebar-1";
    }
    else if (!$content['sidebar_2']) {
        $add_css_classes = " panel-osu-lpage-no-sidebar-2";
    }
}

 ?>
<div class="panel-display panel-osu-lpage clearfix<?php print $add_css_classes; ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
    <div class="panel-panel panel-fullwidth panel-headline col-full">
        <div class="inside"><?php print $content['headline']; ?></div>
    </div>

    <div class="panel-panel panel-fullwidth panel-spotlight col-full">
        <div class="inside"><?php print $content['spotlight']; ?></div>
    </div>


    <div class='panel-col-wrapper osu-equalize-child-columns'> 
        <div class="panel-panel panel-content col-half">
            <div class="inside"><?php print $content['content']; ?></div>
        </div>

        <div class="panel-panel panel-sidebar panel-sidebar-1 col-narrow">
            <div class="inside"><?php print $content['sidebar_1']; ?></div>
        </div>

        <div class="panel-panel panel-sidebar panel-sidebar-2 col-narrow">
            <div class="inside"><?php print $content['sidebar_2']; ?></div>
        </div>

    </div>
</div>
