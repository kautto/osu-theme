<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('2 Full-width, 3 Columns (Default)'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_lpage',
  'regions' => array(
  	'headline' => t('Headline'),
  	'spotlight' => t('Spotlight'),
  	'content' => t('Content'),
  	'sidebar_1' => t('First Sidebar'),
  	'sidebar_2' => t('Second Sidebar')
  ),
);


