<?php
/**
 * @file
 * Template for the 3 column panel layout for content pages.
 *
 * This template provides a the layout for content pages. A left and right sidebar exist around the main 
 * content area. If the sidebars are empty, the content area is allowed to expand to cover the missing space.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['sidebar_left']: Content in the left column.
 *   - $content['content_middle']: Content in the middle column.
 *   - $content['sideabar_right']: Content in the right column.
 */

 /*
  * Additional CSS classes that are added in to adjust the width of elements in case the sidebar(s) are empty.
  */

$add_css_classes = "";

if ((!$content['sidebar_1']) || (!$content['sidebar_2'])) {
    if ((!$content['sidebar_1']) && (!$content['sidebar_2'])) {
        $add_css_classes = " panel-osu-3col-no-sidebars";
    }
    else if (!$content['sidebar_1']) {
        $add_css_classes = " panel-osu-3col-no-sidebar-1";
    }
    else if (!$content['sidebar_2']) {
        $add_css_classes = " panel-osu-3col-no-sidebar-2";

    }
}

 ?>

<div class="panel-display panel-osu-3col clearfix<?php if ($add_css_classes) { print " " . $add_css_classes; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="panel-panel panel-sidebar panel-sidebar-1 col-narrow">
    <div class="inside"><?php print $content['sidebar_1']; ?></div>
  </div>

  <div class="panel-panel panel-content col-half">
    <div class='breadcrumb-container'>
      <?php 
      $breadcrumb = theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));
      print render($breadcrumb); 
      ?>
    </div>

    <div class="inside"><?php print $content['content']; ?></div>
  </div>

  <div class="panel-panel panel-sidebar panel-sidebar-2 col-narrow">
    <div class="inside"><?php print $content['sidebar_2']; ?></div>
  </div>

</div>
