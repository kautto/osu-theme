<?php
/**
 * @file
 * Template for the 2 fullwidth row + 4 column panel layout for landing pages.
 *
 */

 ?>
<div class="panel-display panel-osu-lpage-4col clearfix">
    <div class="panel-panel panel-fullwidth panel-headline col-full">
        <div class="inside"><?php print $content['headline']; ?></div>
    </div>

    <div class="panel-panel panel-fullwidth panel-spotlight col-full">
        <div class="inside"><?php print $content['spotlight']; ?></div>
    </div>
    <div class='panel-col-wrapper osu-equalize-child-columns'>
        <div class="panel-panel panel-sidebar panel-sidebar-1 col-narrow">
            <div class="inside"><?php print $content['sidebar_1']; ?></div>
        </div>

        <div class="panel-panel panel-sidebar panel-sidebar-2 col-narrow">
            <div class="inside"><?php print $content['sidebar_2']; ?></div>
        </div>

        <div class="panel-panel panel-sidebar panel-sidebar-3 col-narrow">
            <div class="inside"><?php print $content['sidebar_3']; ?></div>
        </div>

        <div class="panel-panel panel-sidebar panel-sidebar-4 col-narrow">
            <div class="inside"><?php print $content['sidebar_4']; ?></div>
        </div>
    </div>

</div>
