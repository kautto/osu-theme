<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('2 Full-width, 4 Columns'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_lpage_4col',
  'regions' => array(
  	'headline' => t('Headline'),
  	'spotlight' => t('Spotlight'),
  	'sidebar_1' => t('First Column'),
  	'sidebar_2' => t('Second Column'),
    'sidebar_3' => t('Third Column'),
    'sidebar_4' => t('Fourth Column')

  ),
);


