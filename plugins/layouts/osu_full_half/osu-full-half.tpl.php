<?php
/**
 * @file
 * Template for the 2 row, full-half layout.
 *
 * We are NOT using flexing in this layout, so we don't need to check the content columns
 * to determine which ones are empty.
 */



 ?>

<div class="panel-display panel-osu-2row-full-half clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>


  <div class="panel-panel panel-content col-full">
    <div class="inside"><?php print $content['content']; ?></div>
  </div>

  <div class='panel-col-wrapper osu-equalize-child-columns'> 

    <div class="panel-panel panel-sidebar panel-sidebar-1 col-half">
      <div class="inside"><?php print $content['sidebar_1']; ?></div>
    </div>

    <div class="panel-panel panel-sidebar panel-sidebar-2 col-half">
      <div class="inside"><?php print $content['sidebar_2']; ?></div>
    </div>

  </div>

</div>
