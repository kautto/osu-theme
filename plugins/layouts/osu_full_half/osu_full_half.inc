<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('1 Full-width, 2 Columns'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_full_half',
  'regions' => array(
    'content' => t('Full-Width Column'),
    'sidebar_1' => t('Left Half-Width Column'),
    'sidebar_2' => t('Right Half-Width Column')
  ),
);


