<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('Flex columns with left menu'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_3col_menuflex',
  'regions' => array(
    'content' => t('Content column'),
    'sidebar_2' => t('Right sidebar')
  ),
);


