<?php
/**
 * @file
 * Template for the 3 column panel layout for content pages.
 *
 * This template provides a the layout for content pages. A left and right sidebar exist around the main
 * content area. If the sidebars are empty, the content area is allowed to expand to cover the missing space.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['sidebar_1']: Content in the left column.
 *   - $content['content']: Content in the middle column.
 *   - $content['sidebar_2']: Content in the right column.
 */

// Forcibly add the menu.
$block = block_load('osu_theme', 'osu_theme_active_submenu');
$block->cache = DRUPAL_NO_CACHE;
$block_array = array($block);
$block = _block_render_blocks($block_array);
$block_renderable = _block_get_renderable_array($block);
$content['sidebar_1'] = render($block_renderable);

 /*
  * Additional CSS classes that are added in to adjust the width of elements in case the sidebar(s) are empty.
  */

// Load defined vars to pass regions through to functions below
$vars = get_defined_vars();

$add_css_classes = "";

/*
* Get the variables for sidebar region names, if they're available.
* The region names (tiles of the areas defined in the .inc file) are used 
* when trying ot determine if a pane is empty, since they're included as an <h3> tag
*/

$region_names = array(
                    'sidebar_1' => FALSE,
                    'sidebar_2' => FALSE,
                    );


if (isset($vars['layout']) && isset($vars['layout']['regions'])) {
  if (isset($vars['layout']['regions']['sidebar_1'])) {
    $region_names['sidebar_1'] = $vars['layout']['regions']['sidebar_1'];
  }
  if (isset($vars['layout']['regions']['sidebar_2'])) {
    $region_names['sidebar_2'] = $vars['layout']['regions']['sidebar_2'];
  }
}



if (osu_theme_is_pane_empty($content['sidebar_1'],$region_names['sidebar_1']) || osu_theme_is_pane_empty($content['sidebar_2'],$region_names['sidebar_2'])) {
  if (osu_theme_is_pane_empty($content['sidebar_1'],$region_names['sidebar_1']) && osu_theme_is_pane_empty($content['sidebar_2'],$region_names['sidebar_2'])) {
    $add_css_classes = " panel-osu-3col-flex-no-sidebars";
  }
  else if (osu_theme_is_pane_empty($content['sidebar_1'],$region_names['sidebar_1'])) {
    $add_css_classes = " panel-osu-3col-flex-no-sidebar-1";
  }
  else if (osu_theme_is_pane_empty($content['sidebar_2'],$region_names['sidebar_2'])) {
    $add_css_classes = " panel-osu-3col-flex-no-sidebar-2";
  }
}


?>


<div class="panel-display panel-osu-3col panel-osu-3col-flex panel-osu-3col-menuflex clearfix<?php if ($add_css_classes) { print " " . $add_css_classes; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="panel-panel panel-sidebar panel-sidebar-1 col-narrow">
    <div class="inside"><?php print $content['sidebar_1']; ?></div>
  </div>

  <div class='panel-panel panel-content-wrapper'>

    <div class='panel-breadcrumb'>
      <?php
      $breadcrumb = theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));
      print render($breadcrumb);
      ?>
    </div>
    <div class='panel-title-pane'><h1><?php print drupal_get_title() ?></h1></div>
    <div class="panel-panel panel-content col-half">
      <div class="inside"><?php print $content['content']; ?></div>
    </div>

    <div class="panel-panel panel-sidebar panel-sidebar-2 col-narrow">
      <div class="inside"><?php print $content['sidebar_2']; ?></div>
    </div>

  </div>

</div>
