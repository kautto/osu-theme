<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('2 Columns (Flex)'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_2col_flex',
  'regions' => array(
    'content' => t('Content'),
    'sidebar' => t('Sidebar')
  ),
);


