<?php
/**
 * @file
 * Template for the 2 column layout; main content and right sidebar.
 *
 */



/*
* Get the variables for sidebar region names, if they're available.
* The region names (tiles of the areas defined in the .inc file) are used
* when trying ot determine if a pane is empty, since they're included as an <h3> tag
*/

$region_names = array(
  'sidebar' => FALSE,
);


if (isset($vars['layout']) && isset($vars['layout']['regions'])) {
  if (isset($vars['layout']['regions']['sidebar'])) {
    $region_names['sidebar'] = $vars['layout']['regions']['sidebar'];
  }
}

$additional_classes = '';

if (osu_theme_is_pane_empty($content['sidebar'],$region_names['sidebar'])) {
  $additional_classes = ' no-sidebar ';
}

?>

<div class="panel-display panel-osu-2col panel-osu-2col-flex<?php print $additional_classes; ?> clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

    <div class="panel-panel panel-content col-wide">
        <div class="inside"><?php print $content['content']; ?></div>
    </div>

    <div class="panel-panel panel-sidebar panel-sidebar col-narrow">
        <div class="inside"><?php print $content['sidebar']; ?></div>
    </div>

</div>
