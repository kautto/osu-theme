<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('2 Columns (Left narrow)'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_2col_left',
  'regions' => array(
    'sidebar_1' => t('Left sidebar'),
    'content' => t('Content column')
  ),
);


