<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('3-Column Flex'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_3col_flex',
  'regions' => array(
    'sidebar_1' => t('Left sidebar'),
    'content' => t('Content column'),
    'sidebar_2' => t('Right sidebar')
  ),
);


