<?php
/**
 * @file
 * Template for the 2 column layout; main content and right sidebar.
 *
 */

?>

<div class="panel-display panel-osu-2col panel-osu-2col-right clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

    <div class="panel-panel panel-content col-wide">
        <div class="inside"><?php print $content['content']; ?></div>
    </div>

    <div class="panel-panel panel-sidebar panel-sidebar-2 col-narrow">
        <div class="inside"><?php print $content['sidebar_2']; ?></div>
    </div>

</div>
