<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('2 Columns (Right narrow)'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_2col_right',
  'regions' => array(
    'content' => t('Content column'),
    'sidebar_2' => t('Right sidebar')
  ),
);


