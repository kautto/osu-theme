<?php

/**
 * Implements hook_panels_layouts.
 */
// Plugin definition
$plugin = array(
  'title' => t('OSU Person'),
  'category' => t('Ohio State University'),
  'icon' => 'preview.png',
  'theme' => 'osu_person',
  'regions' => array(
    'sidebar_1' => t('Left sidebar'),
    'content' => t('Content'),
    'accordion' => t('Accordion'),
  ),
);
