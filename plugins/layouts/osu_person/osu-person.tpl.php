<?php
/**
 * @file
 * Template for the 3 column panel layout for content pages.
 *
 * This template provides a the layout for content pages. A left and right sidebar exist around the main
 * content area. If the sidebars are empty, the content area is allowed to expand to cover the missing space.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['sidebar_1']: Content in the left column.
 *   - $content['content']: Content in the middle column.
 *   - $content['sidebar_2']: Content in the right column.
 */

 /*
  * Additional CSS classes that are added in to adjust the width of elements in case the sidebar(s) are empty.
  */

$add_css_classes = "";

if (osu_theme_is_pane_empty($content['sidebar_1'])) {
  $add_css_classes = " panel-osu-3col-flex-no-sidebars ";
}
else {
  $add_css_classes = " panel-osu-3col-flex-no-sidebar-2 ";
}

?>


<div class="panel-display panel-osu-3col panel-osu-3col-flex clearfix  <?php if ($add_css_classes) { print " " . $add_css_classes; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

<?php 
if (!osu_theme_is_pane_empty($content['sidebar_1'])) { 
?>
  <div class="panel-panel panel-sidebar panel-sidebar-1 col-narrow">
    <div class="inside"><?php print $content['sidebar_1']; ?></div>
  </div>
<?php 
  }
?>

  <div class='panel-panel panel-content-wrapper'>

    <div class='panel-breadcrumb'>
      <?php
      $breadcrumb = theme('breadcrumb', array('breadcrumb' => drupal_get_breadcrumb()));
      print render($breadcrumb);
      ?>
    </div>

    <?php
    /*
     The local task tabs (edit/view/draft/etc) are being rendered here instead of in the main
     layout theme to allow better in-line placement with the content column. The tabs are set to
     NOT render in the main layout file for any panels-layout-based pages, so it needs to be here
     to make sure there are proper links available.
     */

    $tabs = menu_local_tabs();
    print render($tabs);

    /*
     We are not using the workbench moderation block at this time.

    if (module_exists('workbench')) {
      // Render workbench moderation block
      $workbench_block = module_invoke('workbench', 'block_view', 'block');
      print render($workbench_block['content']);
    }
    */
    ?>


    <div class="panel-panel panel-content col-wide">
      <div class="inside">
        <div class="osu-person">

          <div class="osu-person-content">
            <?php print $content['content']; ?>
          </div>

          <div class="dropshadow"></div>

          <div class="osu-person-accordion">
            <?php print $content['accordion']; ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>