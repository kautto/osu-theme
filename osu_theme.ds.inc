<?php
/**
 * @file
 * osu_theme.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function osu_theme_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'active_page_submenu';
  $ds_field->label = 'Active Page Submenu';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'osu_theme|osu_theme_active_submenu',
    'block_render' => '1',
  );
  $export['active_page_submenu'] = $ds_field;

  return $export;
}
